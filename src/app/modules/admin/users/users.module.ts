import { NgModule } from "@angular/core";
import { UserListComponent } from "./user-list/user-list.component";
import { Route, RouterModule } from "@angular/router";
import { HttpClientModule } from "@angular/common/http";
import { CommonModule } from "@angular/common";
import { MatTableModule } from "@angular/material/table";
import { MatSortModule } from "@angular/material/sort";
import { MatPaginatorModule } from "@angular/material/paginator";
import { MatFormFieldModule } from "@angular/material/form-field";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MatInputModule } from "@angular/material/input";
import { MatIconModule } from "@angular/material/icon";
import { UserFormComponent } from "./user-form/user-form.component";
import { MatButtonModule } from "@angular/material/button";
import { MatSnackBarModule } from "@angular/material/snack-bar";
import { PostListComponent } from "./post-list/post-list.component";
import { MatDialogModule } from "@angular/material/dialog";
import { FuseConfirmationModule } from "@fuse/services/confirmation";
import { MatCardModule } from "@angular/material/card";
import { PostDetailComponent } from './post-detail/post-detail.component';
import { AddPostComponent } from './add-post/add-post.component';


const userRoutes: Route[] = [
  {
    path: "",
    component: UserListComponent,
  },
  {
    path: ":id",
    component: UserFormComponent,
  },
  {
    path: "add",
    component: UserFormComponent,
  },
  {
    path: ":id/posts",
    component: PostListComponent,
  },
  {
    path: ":id/posts/:id",
    component: PostDetailComponent,
  },
];

@NgModule({
  declarations: [UserListComponent, UserFormComponent, PostListComponent, PostDetailComponent, AddPostComponent],
  imports: [
    RouterModule.forChild(userRoutes),
    CommonModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    MatButtonModule,
    MatSnackBarModule,
    MatDialogModule,
    FuseConfirmationModule,
    MatCardModule
  ],
})
export class UsersModule {}
