import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from "environments/environment";
import { Observable } from "rxjs";
import { IPost } from "./post-list/ipost";
import { IUser } from "./user-list/iuser";

@Injectable({
  providedIn: "root",
})
export class UsersService {
  private baseURL: string = environment.apiURL;

  constructor(private http: HttpClient) {}

  getUser(userId: string): Observable<IUser> {
    return this.http.get<IUser>(`${this.baseURL}/users/${userId}`);
  }

  getUsers(): Observable<IUser[]> {
    return this.http.get<IUser[]>(`${this.baseURL}/users`);
  }

  saveUser(user: IUser, editing: boolean): Observable<IUser> {
    if (!editing) {
      return this.http.post<IUser>(`${this.baseURL}/users`, user);
    } else {
      return this.http.put<IUser>(`${this.baseURL}/users/${user.id}`, user);
    }
  }

  deleteUser(id: string): Observable<IUser> {
    return this.http.delete<IUser>(`${this.baseURL}/users/${id}`);
  }

  getPost(userId:string, id:string):Observable<IPost> {
    return this.http.get<IPost>(`${this.baseURL}/users/${userId}/posts/${id}`);
  }

  getPosts(id: string): Observable<IPost[]> {
    return this.http.get<IPost[]>(`${this.baseURL}/users/${id}/posts`)
  }

  createPost(id: string, post: IPost): Observable<IPost> {
    return this.http.post<IPost>(`${this.baseURL}/users/${id}/posts`, post);
  }
}
