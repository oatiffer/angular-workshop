import { Component, OnDestroy, OnInit, ViewChild } from "@angular/core";
import { UsersService } from "../users.service";
import { IUser } from "./iuser";
import { MatTableDataSource } from "@angular/material/table";
import { Subject, takeUntil } from "rxjs";
import { MatSort, Sort } from "@angular/material/sort";
import { LiveAnnouncer } from "@angular/cdk/a11y";
import { MatPaginator } from "@angular/material/paginator";
import { ActivatedRoute, Router } from "@angular/router";
import { MatSnackBar } from "@angular/material/snack-bar";
import { FuseConfirmationService } from "@fuse/services/confirmation";
import { MatDialogRef } from "@angular/material/dialog";
import { FuseConfirmationDialogComponent } from "@fuse/services/confirmation/dialog/dialog.component";

@Component({
  selector: "app-user-list",
  templateUrl: "./user-list.component.html",
  styleUrls: ["./user-list.component.scss"],
})
export class UserListComponent implements OnInit, OnDestroy {
  private _unsubscribeAll: Subject<boolean> = new Subject();
  private _filterTerm: string = "";
  private _dialogRef: MatDialogRef<FuseConfirmationDialogComponent>;

  postsMeta = { userId: "", enableButton: false, rowSelected: false };
  dataSource: MatTableDataSource<IUser>;
  displayedColumns: string[] = [
    "id",
    "name",
    "username",
    "email",
    "phone",
    "website",
    "actions",
  ];
z
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    private _usersService: UsersService,
    private _liveAnnouncer: LiveAnnouncer,
    private _router: Router,
    private _activatedRoute: ActivatedRoute,
    private _snackbar: MatSnackBar,
    private _fuseConfirmationService: FuseConfirmationService
  ) {}

  get filterTerm(): string {
    return this._filterTerm;
  }

  set filterTerm(value: string) {
    this.dataSource.filter = value;
  }

  ngOnInit(): void {
    this._usersService
      .getUsers()
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe({
        next: (users: IUser[]) => {
          this.dataSource = new MatTableDataSource(users);
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
        },
      });
  }

  ngOnDestroy(): void {
    this._unsubscribeAll.next(true);
    this._unsubscribeAll.unsubscribe();
  }

  announceSortChange(sortState: Sort) {
    if (sortState.direction) {
      this._liveAnnouncer.announce(`Sorted ${sortState.direction}ending`);
    } else {
      this._liveAnnouncer.announce("Sorting cleared");
    }
  }

  addUser() {
    this._router.navigate(["add"], { relativeTo: this._activatedRoute });
  }

  editUser(id: string) {
    this._router.navigate([id], { relativeTo: this._activatedRoute });
  }

  deleteUser(id: string, event) {
    event.stopPropagation();
    this._dialogRef = this._fuseConfirmationService.open({
      title: "Confirm!",
      message: "Confirm deletion",
      actions: {
        confirm: {
          label: "Delete",
          color: "warn",
        },
      },
    });

    this._dialogRef.afterClosed().subscribe((userSeletion) => {
      if (userSeletion === "confirmed") {
        this._usersService
          .deleteUser(id)
          .pipe(takeUntil(this._unsubscribeAll))
          .subscribe((user: IUser) => {
            this._snackbar.open("User deleted succesfuly", "", {
              duration: 3500,
            });
          });
      }
    });
  }

  enablePosts(id: string) {
    this.postsMeta = {
      userId: id,
      enableButton: true,
      rowSelected: true,
    };
  }

  listPosts() {
    this._router.navigate([`${this.postsMeta.userId}/posts`], {
      relativeTo: this._activatedRoute,
    });
  }
}
