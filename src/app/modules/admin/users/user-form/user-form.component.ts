import { Component, OnDestroy, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { Subject, takeUntil } from "rxjs";
import { IUser } from "../user-list/iuser";
import { UsersService } from "../users.service";
import { User } from "./user";
import { MatSnackBar } from "@angular/material/snack-bar";

@Component({
  templateUrl: "./user-form.component.html",
  styleUrls: ["./user-form.component.scss"],
})
export class UserFormComponent implements OnInit, OnDestroy {
  private _unsubscribeAll: Subject<boolean> = new Subject();

  user: IUser = new User();
  editing: boolean = false;
  errorMessage: string = "";

  userForm: FormGroup = this._fb.group({
    name: ["", [Validators.required, Validators.maxLength(50)]],
    username: ["", [Validators.required, Validators.maxLength(8)]],
    email: ["", [Validators.required, Validators.email]],
    phone: "",
    website: "",
    address: this._fb.group({
      street: ["", [Validators.required]],
      suite: ["", [Validators.required]],
      city: ["", [Validators.required]],
      zipcode: ["", [Validators.required]],
    }),
    company: this._fb.group({
      name: "",
      catchPhrase: "",
    }),
  });

  get name() {
    return this.userForm.get("name");
  }
  get username() {
    return this.userForm.get("username");
  }
  get email() {
    return this.userForm.get("email");
  }
  get street() {
    return this.userForm.get("street");
  }
  get suite() {
    return this.userForm.get("suite");
  }
  get city() {
    return this.userForm.get("city");
  }
  get zipCode() {
    return this.userForm.get("zipCode");
  }

  constructor(
    private _usersService: UsersService,
    private router: Router,
    private _activatedRoute: ActivatedRoute,
    private _fb: FormBuilder,
    private _snackbar: MatSnackBar
  ) {}

  ngOnInit(): void {
    this._activatedRoute.params.subscribe((urlParams: any) => {
      if (urlParams.id !== "add") {
        this.editing = true;

        this._usersService
          .getUser(urlParams.id)
          .pipe(takeUntil(this._unsubscribeAll))
          .subscribe((user) => {
            this.user = user;
            this.populateForm();
          });
      }
    });
  }

  ngOnDestroy(): void {
    this._unsubscribeAll.next(true);
    this._unsubscribeAll.unsubscribe();
  }

  populateForm(): void {
    this.userForm.setValue({
      name: this.user.name,
      username: this.user.username,
      email: this.user.email,
      phone: this.user.phone,
      website: this.user.website,
      address: {
        street: this.user.address.street,
        suite: this.user.address.suite,
        city: this.user.address.city,
        zipcode: this.user.address.zipcode,
      },
      company: {
        name: this.user.company.name,
        catchPhrase: this.user.company.catchPhrase,
      },
    });
  }

  saveUser() {
    if (this.userForm.valid) {
      if (!this.editing) {
        this.user = { ...this.userForm.value };
      } else {
        this.user = { id: this.user.id, ...this.userForm.value };
      }

      this._usersService
        .saveUser(this.user, this.editing)
        .pipe(takeUntil(this._unsubscribeAll))
        .subscribe((user: IUser) => {
          this.user = user;
          !this.editing
            ? this._snackbar.open("User created succesfuly", "", {
                duration: 3500,
              })
            : this._snackbar.open("User updated succesfuly", "", {
                duration: 3500,
              });
          !this.editing ? this.returnHome() : null;
        });
    } else {
      this.errorMessage = "Please correct the validation errors.";
    }
  }

  returnHome() {
    this.router.navigate([""]);
  }
}
