export class User {
  constructor(
    public id: number = null,
    public name: string = "",
    public username: string = "",
    public email: string = "",
    public address = {
      street: "",
      suite: "",
      city: "",
      zipcode: "",
      geo: {
        lat: "",
        lng: "",
      },
    },
    public phone: string = "",
    public website: string = "",
    public company = {
      name: "",
      catchPhrase: "",
      bs: "",
    }
  ) {}
}
