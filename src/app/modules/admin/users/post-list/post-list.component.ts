import { Component, OnInit } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { MatSnackBar } from "@angular/material/snack-bar";
import { ActivatedRoute, Router } from "@angular/router";
import { Subject, takeUntil } from "rxjs";
import { AddPostComponent } from "../add-post/add-post.component";
import { PostDetailComponent } from "../post-detail/post-detail.component";
import { UsersService } from "../users.service";
import { IPost } from "./ipost";

@Component({
  templateUrl: "./post-list.component.html",
  styleUrls: ["./post-list.component.scss"],
})
export class PostListComponent implements OnInit {
  private _unsubscribeAll: Subject<boolean> = new Subject();

  postList: IPost[];
  postAuthor: string;
  postAuthorId: string;

  constructor(
    private _router: Router,
    private _usersService: UsersService,
    private _activatedRoute: ActivatedRoute,
    public dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this._activatedRoute.params.subscribe((urlParams: any) => {
      this._usersService
        .getPosts(urlParams.id)
        .pipe(takeUntil(this._unsubscribeAll))
        .subscribe((posts: IPost[]) => {
          this.postList = posts;
          this.getAuthor(posts[0].userId);
        });
    });
  }

  ngOnDestroy(): void {
    this._unsubscribeAll.next(true);
    this._unsubscribeAll.unsubscribe();
  }

  getAuthor(userId: number) {
    this._usersService
      .getUser(userId.toString())
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe((user) => {
        this.postAuthor = user.name;
        this.postAuthorId = user.id.toString();
      });
  }

  getPost(post: IPost) {
    this.dialog.open(PostDetailComponent, {
      data: post,
    });
  }

  addPost() {
    this.dialog.open(AddPostComponent, { data: this.postAuthorId });
  }

  returnHome() {
    this._router.navigate([""]);
  }
}
