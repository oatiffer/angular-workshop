import { Component, Inject, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { MatSnackBar } from "@angular/material/snack-bar";
import { Subject, takeUntil } from "rxjs";
import { IPost } from "../post-list/ipost";
import { IUser } from "../user-list/iuser";
import { UsersService } from "../users.service";

@Component({
  selector: "app-add-post",
  templateUrl: "./add-post.component.html",
  styleUrls: ["./add-post.component.scss"],
})
export class AddPostComponent implements OnInit {
  private _unsubscribeAll: Subject<boolean> = new Subject();

  post: IPost;

  postForm: FormGroup = this._fb.group({
    title: ["", [Validators.required]],
    body: ["", [Validators.required]],
  });

  get title() {
    return this.postForm.get("title");
  }

  get body() {
    return this.postForm.get("body");
  }

  constructor(
    private _snackbar: MatSnackBar,
    private _fb: FormBuilder,
    private _usersService: UsersService,
    public dialogRef: MatDialogRef<AddPostComponent>,
    @Inject(MAT_DIALOG_DATA) public data: string
  ) {}

  ngOnInit(): void {}

  savePost() {
    this.post = this.postForm.value;

    this._usersService
        .createPost(this.data, this.post)
        .pipe(takeUntil(this._unsubscribeAll))
        .subscribe((post: IPost) => {
          console.log(post);
          this.dialogRef.close();
          this._snackbar.open("Post created succesfuly", "", {
            duration: 3500,
          });
        });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
